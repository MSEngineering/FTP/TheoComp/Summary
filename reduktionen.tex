
\section{Reduktionen}

\begin{breakbox}
\textbf{Turing-Reduktion:}
\newline $A \leq_T B$ (lässt sich Turing-reduzieren), wenn folgende Dinge existieren:
\begin{enumerate}
	\item Algorithmus $Alg_B$, der Problem $B$ löst.
	\item Algorithmus $Alg_A$, der Problem $A$ löst, indem er $Alg_B$ als Unterprogramm aufruft.
	\item Polynome $p, q, r$, sodass folgende Abschätzung der Laufzeiten für Eingaben der Länge $n$ gilt: $t_{Alg_A}(n) \leq p(n) + q(n) t_{Alg_B}(r(n))$, wobei $p(n)$ die Laufzeit von $Alg_A$ und $q(n)$ die Anzahl Aufrufe von $Alg_B$ während Laufzeit von $Alg_A$ abschätzt. $r(n)$ schätzt Inputlänge der Eingabe für Aufruf von $Alg_B$ ab.
\end{enumerate}
Das Problem $A$ ist Turing-reduzierbar auf das Problem $B$, wenn es eine Orakel-Turing-Maschine relativ zum Problem $B$ gibt, die in polynomialer Laufzeit das Problem $A$ löst.
\end{breakbox}

\begin{breakbox}
\textbf{Definition:}
\newline Eine Orakel-TM relativ zum Problem B ist eine (mind.) 3-Band-TM mit einem Eingabeband, einem Berechnungsband und einem Orakelband. Sie besitzt einen speziellen Zustand, den Orakelantwortzustand. Wenn die Maschine in diesem Zustand ist und einen Schritt macht, wird das Wort auf dem Orakelband als Input für das Problem B gedeutet und in einem Schritt durch die Lösung ersetzt.
\end{breakbox}

\begin{breakbox}
\textbf{Beispiele:}
\begin{itemize}
	\item $L \leq_T \overline{L}$, für alle Sprachen.
	\item Es gilt: $(VC) \leq_T (IS) \leq_T (Clique) \leq_T (VC)$.
\end{itemize}
$(VC)$ = Vertex-Cover
\newline $(IS)$ = Independent Set = Anticlique
\begin{center}
\includegraphics[width=.15\textwidth]{images/independent_set}
\end{center}
$(Clique)$ = $V$, sodass jeder Knoten der Clique mit jedem anderen Knoten einer Clique durch eine Kante verbunden ist.
\begin{center}
\includegraphics[width=.15\textwidth]{images/clique}
\end{center}
\end{breakbox}

\begin{breakbox}
\textbf{Karp-Reduktion:}
\newline $L_1 \leq_p L_2$ (Karp- oder poynomial-reduzierbar), wenn gilt: $\exists f : \Sigma^* \rightarrow \Sigma^*$ mit
\begin{enumerate}
	\item $\forall x \in \Sigma^* : x \in L_1 \Leftrightarrow f(x) \in L_2$.
	\item $f$ ist in polynomieller Laufzeit T-berechenbar.
\end{enumerate}
Merke: $\leq_p$ ist reflexiv und transitiv.
\end{breakbox}

\begin{breakbox}
\textbf{Bemerkung:}
\newline Es gilt:
\begin{center}
$L \leq_p L' \rightarrow L \leq_T L'$.
\end{center}
Die Umkehrung gilt aber nicht. Die Karp-Reduktion ist ein Spezialfall der Touring-Reduktion, bei dem das Orakel genau einmal (am Ende einer Berechnung) befragt und die Antwort (Ja oder Nein) unverändert ausgegeben wird.
\end{breakbox}

\begin{breakbox}
\textbf{Lemma:}
\newline Wenn $L_1 \leq_p L_2$ gilt, folgt
\begin{enumerate}
	\item $L_2 \in P \Rightarrow L_1 \in P$ und
	\item $L_1 \notin P \Rightarrow L_2 \notin P$.
\end{enumerate}
\end{breakbox}

\begin{breakbox}
\textbf{Definitionen:}
\begin{enumerate}
	\item $L$ heisst NP-hart $\Leftrightarrow \forall \widetilde{L} \in NP : \widetilde{L} \leq_p L$.
	\item $L$ heisst NP-vollständig: \begin{enumerate}
			\item $L$ ist NP-hart.
			\item $L \in$ NP.
		\end{enumerate} 
\end{enumerate}
\includegraphics[width=.3\textwidth]{images/complexity_classes_venn}
Merke: Ein NP-hartes Problem ist mindestens so schwer wie alle Probleme in NP.
\end{breakbox}

\begin{breakbox}
\textbf{NPC:}
\begin{itemize}
	\item Menge der NP-vollständigen Sprachen.
	\item Sprachen in NPC haben die schwierigsten Entschiedungsprobleme in NP.
	\item Wenn $L_1, L_2 \in$ NPC, dann gilt $L_1 \leq_p L_2$ und $L_2 \leq_p L_1$.
	\item Wenn eine NP-vollständige Sprache existiert, die in P liegt, dann gilt P = NP.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Komplementärklasse:}
\newline Gegeben eine Sprachklasse $\mathcal{C} \subset P(\Sigma^*)$. Dann ist die zu $\mathcal{C}$ komplemetäre Sprachklasse co$\mathcal{C}$ definiert als:
\begin{center}
	$\{L \subset \Sigma^* | \exists M \in \mathcal{C} : L = \Sigma^* \setminus M\}$.
\end{center}
\end{breakbox}

\begin{breakbox}
\textbf{Beispiele:}
\begin{itemize}
	\item coNP := Menge der Sprachen, deren Komplement in NP liegt. Für diese Sprachen gilt, dass es ein Zertifikat gibt, das aussagt, dass es keine Lösung gibt für das Problem.
	\item NP $\cap$ coNP := Menge der Sprachen, für die man definitiv entscheiden kann, ob ein Wort in der Sprache ist oder ob es gar keine Lösung gibt.
	\item Euler-Path $\in$ NP $\cap$ coNP. Hamilton-Path $\notin$ NP $\cap$ coNP.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{SAT:}
\begin{itemize}
	\item Gegeben: $\phi$, eine aussagenlogische Formel in konjunktiver Normalform.
	\item Frage: Ist $\phi$ erfüllbar?
\end{itemize}
Merke: SAT $\in$ NPC.
\end{breakbox}

\begin{breakbox}
\textbf{Graphfärbung (kGF):}
\begin{itemize}
	\item Gegeben: Graph $G = (V, E)$.
	\item Frage: Gibt es eine $k$-Färbung von $G$?
\end{itemize}
Merke: $k$-Färbung = Funktion $F : V \rightarrow \{1, 2, ..., k\}$ mit der Eigenschaft, dass für alle benachbarten Knoten $v_1, v_2 \in V$ gilt: $F(v_1) \neq F(v_2)$.
\end{breakbox}

\begin{breakbox}
\textbf{Subset-Sum-Problem:}
\begin{itemize}
	\item Gegeben: $n + 1$ positive ganze Zahlen $a_1, a_2, ..., a_n, t$.
	\item Gesucht: $x_1, ... x_n \in \{0, 1\}$, sodass $t = \sum_{i = 1}^n x_i a_i$ gilt.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Notation:}
\begin{itemize}
	\item kGF($d$) := Menge der Graphen mit $max \{deg v | v \in V\} \leq d$, die mit $k$ Farben färbbar sind.
	\item kGF$_{pl}$ := Menge der planaren Graphen, die mit $k$ Farben färbbar sind.
	\item kGF$_{pl}(d)$ := Menge der planaren Graphen, die mit $k$ Farben färbbar sind und deren maximaler Grad höchstens $d$ ist.
\end{itemize}
\end{breakbox}

\begin{breakbox}
\textbf{Beispiele von Reduktionen:}
\begin{itemize}
	\item SAT $\leq_p$ 3SAT.
	\item 3SAT $\leq_p$ Subset-Sum-Problem.
	\item 3SAT $\leq_p$ Clique-Problem.
	\item 3SAT $\leq_p$ kGF (für $k \geq 3$).
	\item 3GF $\leq_p$ 3GF$_{pl} \leq_p$ 3GF$_{pl}(4)$.
	\item Clique $\leq_p$ IS $\leq_p$ VC $\leq_p$ Clique (für die jeweiligen Entscheidungsprobleme).
\end{itemize}
Diese Probleme sind $\in$ NPC.
\end{breakbox}

\begin{breakbox}
\textbf{Reduktion von SAT auf 3SAT:}
\newline Sei $\phi = k_1 \wedge k_2 \wedge \cdots \wedge k_r$.
Um diese Formel in polynomialer Zeit in 3SAT umzuwandeln, können folgende Regeln angewendet werden:
\begin{itemize}
	\item $k_i = l$ wird $\tilde{k}_i = l \vee l \vee l$.
	\item $k_i = l_1 \vee l_2$ wird $\tilde{k}_i = l_1 \vee l_2 \vee l_2$.
	\item $k_i = l_1 \vee l_2 \vee l_3$ bleibt.
\end{itemize}
Klauseln, die mehr als drei Literale haben ($k_i = l_1 \vee l_2 \vee \cdots \vee l_n$), werden wie folgt aufgeteilt:
\begin{itemize}
	\item $k_{i1} := l_1 \vee l_2 \vee y_{i1}$
	\item $k_{i2} := \neg y_{i1} \vee l_3 \vee y_{i2}$
	\item $k_{i3} := \neg y_{i2} \vee l_4 \vee y_{i3}$
	\item $\cdots$
	\item $k_{i,n-3} := \neg y_{i,n-4} \vee l_{n-2} \vee y_{i,n-3}$
	\item $k_{i,n-2} := \neg y_{i,n-3} \vee l_{n-1} \vee l_n$
\end{itemize}
Zu zeigen ist nun:
$I_w(k_i) = 1 \Leftrightarrow \exists$ Erweiterung der Belegung $w$ für die Variablen $y_{i1}, y_{i2}, \cdots, y_{i,n-3}$ mit $I_w(k_{i1} \wedge k_{i2} \wedge \cdots \wedge k_{i,n-2}) = 1$.
Dazu müssen vier Fälle unterschieden werden:
\begin{enumerate}
	\item Es gilt $w(l_1) = 1$ oder $w(l_2) = 1$. Dann setzen wir $w(y_{ij}) = 0, \forall j$.
	\item Es gilt $w(l_{n-1}) = 1$ oder $w(l_n) = 1$. Dann setzen wir $w(y_{ij}) = 1, \forall j$.
	\item Es gilt $w(l_j) = 1$ für $2 < j < n - 1$. Dann setzen wir $w(y_{i1}) = w(y_{i2}) = \cdots = w(y_{i,j-2}) = 1$ und $w(y_{i,j-1}) = w(y_{ij}) = \cdots w(y_{i,n-3}) = 0$.
	\item Für den Fall $w(l_1) = w(l_2) = \cdots = w(l_n) = 0$ müssen wir zeigen, dass es keine Erweiterung der Belegung $w$ gibt, so dass $I_w(k_{i1} \wedge k_{i2} \wedge \cdots \wedge k_{i,n-2}) = 1$ gilt. Wenn man die Liste ($k_{i1}$ bis $k_{i,n-2}$) mit einer beliebigen Belegung für die Variablen $y_{i1}, y_{i2}, \cdots, y_{i,n-3}$ durchgeht, erhält man einen Widerspruch.
\end{enumerate}
Damit ist die Reduktion gezeigt. \hfill $\square$
\end{breakbox}

\begin{breakbox}
\textbf{Beispiel:}
\newline Gegeben sei die aussagenlogische Formel:
$\phi = (\neg x_1 \vee x_2 \vee x_3) \wedge x_1 \wedge (x_1 \vee x_2 \vee \neg x_3 \vee x_4 \vee \neg x_5) \wedge (\neg x_1 \vee \neg x_2 \vee \neg x_4 \vee x_5 \vee \neg x_7)$.
\newline Nach Anwendung der oben beschriebenen Regeln, ist die Formel: $\phi = k_1 \wedge \tilde{k_2} \wedge k_{31} \wedge k_{32} \wedge k_{33} \wedge k_{41} \wedge k_{42} \wedge k_{43}$ mit:
\begin{itemize}
	\item $k_1 = (\neg x_1 \vee x_2 \vee x_3)$
	\item $\tilde{k_2} = (x_1 \vee x_1 \vee x_1)$
	\item $k_{31} = (x_1 \vee x_2 \vee y_{31})$
	\item $k_{32} = (\neg y_{31} \vee \neg x_3 \vee y_{32})$
	\item $k_{33} = (\neg y_{32} \vee x_4 \vee \neg x_5)$
	\item $k_{41} = (\neg x_1 \vee \neg x_2 \vee y_{41})$
	\item $k_{42} = (\neg y_{41} \vee \neg x_4 \vee y_{42})$
	\item $k_{43} = (\neg y_{42} \vee x_5 \vee \neg x_7)$
\end{itemize}
Eine gültige Belegung ist zum Beispiel $w(x_1) = 1$,  $w(x_2) = 1$, $w(x_3) = 0$, $w(x_4) = 0$, $w(x_5) = 1$, $w(x_6) = 0$, $w(x_7) = 0$.
\end{breakbox}

\begin{breakbox}
\textbf{Beispiel für Reduktion von 3SAT auf Subset-Sum:}
\newline $\phi = (x_1 \vee x_2 \vee x_3) \wedge (\neg x_2 \vee x_3 \vee \neg x_4) \wedge (\neg x_1 \vee x_2 \vee x_4)$.

Für jede logische Variable $x_i$ haben wir zwei Variablen $y_i$ und $z_1$ eingeführt. Die Variable $y_i$ steht $x_i$ und $z_i$ für $x_i$. Kommt das Literal
$x_i$ in der Klausel $c_j$ vor, bekommt der Wert von $y_i$ in der Spalte von $c_j$ die Ziffer Eins, sonst die Ziffer Null. Falls $\neg x_i$
in der Klausel $c_j$ vorkommt, bekommt der Wert von $z_i$ in der Spalte von $c_j$ die Ziffer 1, sonst die Ziffer Null.
\newline Die Variable $t$ ist so belegt, dass jede logische Variable den Wert 0 bzw. 1 haben muss und in jeder Klausel mind. ein Literal
positiv belegt ist: $t=1111333$.

\begin{tabular}{| c | c c c c | c c c |} 
\hline
      & $x_1$ & $x_2$ & $x_3$ & $x_4$ & $c_1$ & $c_2$ & $c_3$ \\ \hline \hline
$y_1$ & 1     & 0     & 0     & 0     & 1     & 0     & 0     \\
$z_1$ & 1     & 0     & 0     & 0     & 0     & 0     & 1     \\ \hline
$y_2$ & 0     & 1     & 0     & 0     & 1     & 0     & 1     \\
$z_2$ & 0     & 1     & 0     & 0     & 0     & 1     & 0     \\ \hline
$y_3$ & 0     & 0     & 1     & 0     & 1     & 1     & 0     \\
$z_3$ & 0     & 0     & 1     & 0     & 0     & 0     & 0     \\ \hline
$y_4$ & 0     & 0     & 0     & 1     & 0     & 0     & 1     \\
$z_4$ & 0     & 0     & 0     & 1     & 0     & 1     & 0     \\ \hline \hline
$g_1$ & 0     & 0     & 0     & 0     & 1     & 0     & 0     \\
$h_1$ & 0     & 0     & 0     & 0     & 1     & 0     & 0     \\ \hline
$g_2$ & 0     & 0     & 0     & 0     & 0     & 1     & 0     \\
$h_2$ & 0     & 0     & 0     & 0     & 0     & 1     & 0     \\ \hline
$g_3$ & 0     & 0     & 0     & 0     & 0     & 0     & 1     \\
$h_3$ & 0     & 0     & 0     & 0     & 0     & 0     & 1     \\ \hline \hline
$t$     & 1     & 1     & 1     & 1     & 3     & 3     & 3   \\ \hline \hline
\end{tabular}
\newline Da nur ein Literal in jeder Klausel positiv sein muss, aber zwei oder drei (3SAT) positiv sein können, führen wir zwei Variablen
$g_i$ und $h_i$ für jede Klausel $c_i$ ein, sonst können wir den Wert 3 in $t$ nicht in jedem Fall für jede Klausel erreichen.

Eine mögliche Lösung (von mehreren möglichen) wäre: $\omega (x_1)=0$, $\omega (x_2)=0$, $\omega (x_3)=1$, $\omega (x_1)=0$. 
Damit wir auf $t$ kommen, müssen wir noch $g_1$, $h_1$, $g_3$ und $h_3$ hinzunehmen. Also
\begin{center}
$z_1 + z_2 + y_3 + z_4 + g_1 + h_1 + g_3 + h_3 = t$
\end{center}
Die Transformation in obige Tabelle erfordert höchstens polynomialen Aufwand, also wurde die Karp-Reduktion bewiesen.
\end{breakbox}

\begin{breakbox}
\textbf{Reduktion von 3SAT auf Clique:}
\newline Gegeben sei die aussagenlogische Formel in 3SAT Form: $\phi = (\neg x_1 \vee x_2 x_3) \wedge (x_1 \vee x_2 \vee \neg x_3) \wedge (\neg x_1 \vee \neg x_2 \vee \neg x_4)$.
\newline Wir bilden den Graphen $G_\phi$. Dazu verbinden wir alle Knoten untereinander mit folgenden beiden Ausnahmen:
\begin{enumerate}
  \item Knoten, die Literalen in derselben Klausel entsprechen, werden nicht verbunden.
  \item Knoten, die Literalen derselben Variable aber mit unterschiedlichem Wahrheitswert entsprechen, werden nicht verbunden. $x_1$ wird also nicht mit $\neg x_1$ wird also \textit{nicht} verbunden, $x_1$ jedoch mit $x_1$ falls es nicht aus derselben Klausel ist.
\end{enumerate}
Das Resultat sieht so aus:
\begin{center}
\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=1.75cm, scale = 0.75, transform shape]
\node[state] 			(A)									{$\neg x_1$};
\node[state] 			(B)	[above of=A]			{$x_2$};
\node[state] 			(C)	[above of=B]			{$x_3$};
\node[state]			(D)	[above right of=C]	{$x_1$};
\node[state]			(E)	[right of=D]				{$x_2$};
\node[state]			(F)	[right of=E]				{$\neg x_3$};
\node[state]			(G)	[below right of=F]		{$\neg x_1$};
\node[state]			(H)	[below of=G]			{$\neg x_2$};
\node[state]			(I)		[below of=H]			{$\neg x_4$};
\path[-] 	(A) 	edge[blue] 	(E)
            			edge[green]	(F)
            			edge	(G)
            			edge	(H)
            			edge[blue]	(I)
         		(B) 	edge	(D)
         				edge	(E)
         				edge	(F)
         				edge	(G)
         				edge	(I)
        		(C) 	edge	(D)
        				edge	(E)
        				edge	(G)
        				edge	(H)
        				edge	(I)
         		(D)	edge	(H)
         				edge	(I)
         		(E)	edge	(G)
         				edge[blue]	(I)
         		(F)	edge	(G)
         				edge	(H)
         				edge[green]	(I);
\end{tikzpicture}
\end{center}
Die zum Graphen $G_\phi$ zugehörige Adjazenzmatrix ist:
\begin{center}
$A_\phi = \begin{pmatrix}
0 & 0 & 0 & 0 & 1 & 1 & 1 & 1 & 1 \\
0 & 0 & 0 & 1 & 1 & 1 & 1 & 0 & 1 \\
0 & 0 & 0 & 1 & 1 & 0 & 1 & 1 & 1 \\
0 & 1 & 1 & 0 & 0 & 0 & 0 & 1 & 1 \\
1 & 1 & 1 & 0 & 0 & 0 & 1 & 0 & 1 \\
1 & 1 & 0 & 0 & 0 & 0 & 1 & 1 & 1 \\
1 & 1 & 1 & 0 & 1 & 1 & 0 & 0 & 0 \\
1 & 0 & 1 & 1 & 0 & 1 & 0 & 0 & 0 \\
1 & 1 & 1 & 1 & 1 & 1 & 0 & 0 & 0 \\
\end{pmatrix}$
\end{center}
Eine gültige Belegung ist zum Beispiel $w(x_1) = 0$,  $w(x_2) = 1$, $w(x_3) = 0$, $w(x_4) = 0$. Diese Belegung entspricht einer oder mehreren Cliquen im Graph $G_\phi$ (siehe oben).
\end{breakbox}

\begin{breakbox}
\textbf{Leichte Teilprobleme:}
\begin{itemize}
	\item 2SAT
	\item 2GF
	\item kGF$(d)$, mit $k > d$
	\item kGF$_{pl}$, mit $k \geq 4$ (Vierfarbensatz)
\end{itemize}
Diese Probleme sind $\in$ P.
\end{breakbox}

\begin{breakbox}
\textbf{2SAT in polynomialer Zeit:}
\newline Gegeben sei die aussagenlogische Formel in 2SAT Form: $\phi = (p_1 \vee p_3) \wedge (p_1 \vee \neg p_4) \wedge (p_2 \vee \neg p_4) \wedge
    (p_2 \vee \neg p_5) \wedge (p_3 \vee \neg p_5) \wedge (p_1 \vee \neg p_6) \wedge
    (p_2 \vee \neg p_6) \wedge (p_3 \vee \neg p_6) \wedge (p_4 \vee p_7) \wedge
    (p_5 \vee p_7) \wedge (p_6 \vee p_7)$.
\newline Wir bauen nun einen Graphen $G_\phi$. Und zwar führen wir für jede Variable $x$ zwei Knoten ein: Einen für $x$ (den Knoten $k(x)$) und einen für $\neg x$ (den Knoten $k(\neg x)$). Die Kanten des Graphen ergeben sich aus den Klauseln.
\newline Für jede Klausel führen wir zwei Kanten ein: Eine Klausel $x \vee y$ kann auf zwei Arten als Implikation geschrieben werden:
\begin{center}
$\neg x \rightarrow y $ oder $\neg y \rightarrow x$.
\end{center}
Für die Implikation $\neg x \rightarrow y$ zeichnen wir eine Kanten von $k(\neg x)$ nach $k(y)$, für die Implikation $\neg y \rightarrow x$ eine Kante von $k(\neg y)$ nach $k(x)$.
\begin{center}
\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=1.75cm, scale = 0.75, transform shape]
\node[state] 			(A)							{$p_1$};
\node[state] 			(B)	[below of=A]		{$p_2$};
\node[state] 			(C)	[below of=B]		{$p_3$};
\node[state]			(D)	[below of=C]	{$p_4$};
\node[state]			(E)	[below of=D]	{$p_5$};
\node[state]			(F)	[below of=E]		{$p_6$};
\node[state]			(G)	[below of=F]		{$p_7$};
\node[state]			(H)	[right of=A]		{$\neg p_1$};
\node[state]			(I)		[below of=H]	{$\neg p_2$};
\node[state]			(J)	[below of=I]		{$\neg p_3$};
\node[state]			(K)	[below of=J]		{$\neg p_4$};
\node[state]			(L)	[below of=K]		{$\neg p_5$};
\node[state]			(M)	[below of=L]		{$\neg p_6$};
\node[state]			(N)	[below of=M]	{$\neg p_7$};
\path[->] 	(D) 	edge[bend left=50]	(A)
         				edge[bend left=50]	(B)
         		(E)	edge[bend left=50]	(B)
         				edge[bend left=50]	(C)
         		(F)	edge[bend left=50]	(A)
         				edge[bend left=50]	(B)
         				edge[bend left=50]	(C)
        		(H) 	edge	(C)
        				edge[bend left=50]	(K)
        				edge[bend left=50]	(M)
         		(I)		edge[bend left=50]	(K)
         				edge[bend left=50]	(L)
         				edge[bend left=50] 	(M)
         		(J)	edge	(A)
         				edge[bend left=50]	(L)
         				edge[bend left=50]	(M)
         		(K)	edge	(G)
         		(L)	edge	(G)
         		(M)	edge	(G)
         		(N)	edge	(D)
         				edge	(E)
         				edge	(F);
\end{tikzpicture}
\end{center}
Wir behaupten nun: Genau dann ist $\phi \in 2SAT$, wenn es im gerichteten Graph keinen Zykel gibt, der mit einem Literal auch dessen Negation enthält. Eine gültige Belegung ist hier zum Beispiel $w(p_1) = 1$,  $w(p_2) = 1$, $w(p_3) = 1$, $w(p_4) = §$, $w(p_5) = 1$, $w(p_6) = 1$, $w(p_7) = 1$.
\end{breakbox}











